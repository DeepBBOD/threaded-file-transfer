import socket
from os import walk
from threading import Thread

clients = []

CHUNK_SIZE = 1024


class ClientListener(Thread):
    def __init__(self, name: str, sock: socket.socket):
        super().__init__(daemon=True)
        self.sock = sock
        self.name = name

    def _close(self):
        clients.remove(self.sock)
        self.sock.close()
        print(self.name + " disconnected")

    def _get_file_name(self):
        try:
            # get name of the file CHUNK_SIZE bytes max
            file_name = self.sock.recv(CHUNK_SIZE)
            # send flag to start file transition
            self.sock.send(bytes("1", "utf8"))
            return file_name.decode("utf-8")
        except BaseException as e:
            print("Filename transition error:", str(e), "Closing the connection {name}".format(name=self.name))

    def write_file(self, data, file_name):
        file_name_trunc = ''.join(file_name.split('.')[:-1])
        for (dirpath, dirnames, filenames) in walk("./storage/"):
            if file_name in filenames:
                file_name = file_name.split(".")[-2] + "_copy{}.".format(
                    sum([1 for i in filenames if file_name_trunc in i])) + file_name.split(".")[-1]
        with open("./storage/{name}".format(name=file_name), "wb") as file:
            file.write(data)
            file.close()

    def run(self):
        temp = bytes("", "utf-8")
        file_name = self._get_file_name()
        if file_name is not None:
            while True:
                data = self.sock.recv(CHUNK_SIZE)
                if data:
                    temp += data
                else:
                    self.write_file(temp, file_name)
                    self._close()
                    return

        self._close()
        return


def main():
    next_name = 1
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(("", 8800))
    sock.listen()
    while True:
        con, addr = sock.accept()
        clients.append(con)
        name = "u" + str(next_name)
        next_name += 1
        print(str(addr) + " connected as " + name)
        ClientListener(name, con).start()


if __name__ == "__main__":
    main()
