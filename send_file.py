import os
import socket
import sys

CHUNK_SIZE = 1024
HOST = sys.argv[2]  # The remote host
PORT = int(sys.argv[3])  # The same port as used by the server


def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


if __name__ == "__main__":

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        file_path = sys.argv[1]
        with open(file_path, "rb") as f:
            s.send(bytes(file_path.split("/")[-1], "utf8"))
            file_name_resp = s.recv(CHUNK_SIZE)
            if file_name_resp != bytes("1", "utf8"):
                raise BaseException("File name transition error!")
            data = f.read(CHUNK_SIZE)
            i = 0
            i_max = max(os.path.getsize(file_path) / CHUNK_SIZE, 1)
            while data:
                i += 1
                print_progress_bar(i, i_max)
                s.send(data)
                data = f.read(CHUNK_SIZE)
        s.close()
